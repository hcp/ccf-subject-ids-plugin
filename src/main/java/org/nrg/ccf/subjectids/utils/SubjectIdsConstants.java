package org.nrg.ccf.subjectids.utils;

/**
 * The Class SubjectIdsConstants.
 */
public class SubjectIdsConstants {
	
	/** The Constant SUBJECT_IDS_TOOL_ID. */
	public static final String SUBJECT_IDS_TOOL_ID = "ccfSubjectIds";
	
	/** The Constant SUBJECT_IDS_TOOL_NAME. */
	public static final String SUBJECT_IDS_TOOL_NAME = "CCF Subject IDs Plugin Preferences";
	
	/** The Constant SUBJECT_IDS_TOOL_DESC. */
	public static final String SUBJECT_IDS_TOOL_DESC = "Manages site and project configurations and settings for the CCF Subject IDs plugin";
	
	/** The Constant SUBJECT_IDS_CONFIG_ENABLED. */
	public static final String SUBJECT_IDS_CONFIG_ENABLED = "ccfSubjectIdsEnabled";
	
	/** The Constant SUBJECT_IDS_CONFIG_SHOWLINK. */
	public static final String SUBJECT_IDS_CONFIG_SHOWLINK = "ccfSubjectIdsShowLink";
	
	/** The Constant SUBJECT_IDS_CONFIG_TEMPLATE. */
	public static final String SUBJECT_IDS_CONFIG_TEMPLATE = "ccfSubjectIdsTemplate";
	
	/** The Constant SUBJECT_IDS_CONFIG_TEMPLATE. */
	public static final String SUBJECT_IDS_CONFIG_NEWTEMPLATE = "ccfSubjectIdsNewTemplate";
	
	/** The Constant SUBJECT_IDS_CONFIG_PROJECTLIST. */
	public static final String SUBJECT_IDS_CONFIG_PROJECTLIST = "ccfSubjectIdsProjectList";
	
	/** The Constant SUBJECT_IDS_CONFIG_CHECKSUM. */
	public static final String SUBJECT_IDS_CONFIG_CHECKSUM = "ccfSubjectIdsChecksum";
	
	/** The Constant SUBJECT_IDS_CONFIG_SHOWLINK. */
	public static final String SUBJECT_IDS_CONFIG_MATCHNUMERIC = "ccfSubjectIdsMatchNumeric";
	
	/** The Constant SUBJECT_IDS_CONFIG_NOTIFIERLIST */
	public static final String SUBJECT_IDS_CONFIG_NOTIFIERLIST = "ccfSubjectIdsNotifierList";

	/** The Constant PROPERTIES_EXCLUSIONS. */
	// Form values we don't want to save in the preferences (mainly the CSRF token value, I think)
	public static final String[] PROPERTIES_EXCLUSIONS = new String[] { "XNAT_CSRF" };

}
