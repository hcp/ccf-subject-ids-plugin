package org.nrg.ccf.subjectids.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.CharUtils;
import org.nrg.ccf.subjectids.exception.SubjectIdsConfigurationException;
import org.nrg.ccf.subjectids.exception.SubjectIdsFormatException;
import org.nrg.ccf.subjectids.exception.SubjectIdsNonUniqueException;
import org.nrg.ccf.subjectids.pojos.TemplateInfo;
import org.nrg.framework.constants.Scope;
import org.nrg.prefs.services.NrgPreferenceService;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.security.helpers.Users;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.ViewManager;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.search.QueryOrganizer;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

/**
 * The Class SubjectIdsUtils.
 */
@Component
public class SubjectIdsUtils {
	
	/** The _pref service. */
	final NrgPreferenceService _prefService;
	
	/**
	 * The Enum CharTypes.
	 */
	public enum CharTypes { LITERAL, USER_ALPHA, USER_ALPHANUM, USER_NUM, SEQUENTIAL, RANDOM, CONSTRAINED_RANDOM, CHECKSUM };
	
	/**
	 * The Enum ChecksumMethod.
	 */
	public enum ChecksumMethod { MOD9710, LASTDIGITS };
	
	/** The _seg start. */
	// processing variables
	int _segStart;
	
	/** The _seg end. */
	int _segEnd;
	
	/** The _user segment number. */
	int _userSegmentNumber;
	
	/** The to be checksummed. */
	StringBuilder toBeChecksummed = new StringBuilder();
	
	/** The _user supplied bits. */
	String[] _userSuppliedBits;
	
	/** The _current user segment. */
	Integer _currentUserSegment;
	
	/** The _subject list. */
	List<String> _subjectList;
	
	/** The _to be checksummed. */
	StringBuilder _toBeChecksummed;
	
	/** The _cs method. */
	ChecksumMethod _csMethod;
	
	/** The random. */
	final Random random = new Random();
	
	/**
	 * Instantiates a new subject ids utils.
	 *
	 * @param prefService the pref service
	 */
	@Autowired
	public SubjectIdsUtils(NrgPreferenceService prefService) {
		_prefService = prefService;
	}
	
	/**
	 * Gets the new subject id.
	 *
	 * @param user the user
	 * @param projectId the project id
	 * @param userSuppliedBits the user supplied bits
	 * @return the new subject id
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 * @throws SubjectIdsFormatException the subject ids format exception
	 * @throws SubjectIdsNonUniqueException the subject ids non unique exception
	 */
	public synchronized String getNewSubjectId(final UserI user, final String projectId, final String[] userSuppliedBits) throws SubjectIdsConfigurationException, SubjectIdsFormatException, SubjectIdsNonUniqueException {
		final ChecksumMethod csMethod = (getProjectChecksumMethod(projectId).equals("MOD9710")) ? ChecksumMethod.MOD9710 : ChecksumMethod.LASTDIGITS;
		return getNewSubjectId(user, projectId, userSuppliedBits, csMethod);
	}
	
	/**
	 * Gets the new subject id.
	 *
	 * @param user the user
	 * @param projectId the project id
	 * @param userSuppliedBits the user supplied bits
	 * @param csMethod the cs method
	 * @return the new subject id
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 * @throws SubjectIdsFormatException the subject ids format exception
	 * @throws SubjectIdsNonUniqueException the subject ids non unique exception
	 */
	public synchronized String getNewSubjectId(final UserI user, final String projectId, final String[] userSuppliedBits, final ChecksumMethod csMethod) throws SubjectIdsConfigurationException, SubjectIdsFormatException, SubjectIdsNonUniqueException {
		final TemplateInfo templateInfo = getProjectTemplateInfo(projectId, true);
		final String template = templateInfo.getTemplate();
		_subjectList = this.getSubjectList(projectId, getProjectTemplateInfo(projectId, false));
		final List<String> _numericList = new ArrayList<>();
		if (matchNumericOnly(projectId)) {
			for (String subjId : _subjectList) {
				_numericList.add(subjId.replaceAll("[^0-9]",""));
		
			}
		}
		for (int k=1; k<_subjectList.size() || k==1; k++) {
			final StringBuilder newId = new StringBuilder();
			_userSuppliedBits = userSuppliedBits;
			_toBeChecksummed = new StringBuilder();
			_userSegmentNumber = 0;
			_segStart = 0;
			_segEnd = 0;
			_csMethod = csMethod;

			final StringBuilder segment = new StringBuilder();
			_segStart = 0; 
			for (int i=0; i<template.length(); i++) {
				final Character thisChar = template.charAt(i);
				final Character nextChar = (i != (template.length()-1)) ? template.charAt(i+1) : null; 
				final CharTypes thisType = getCharType(thisChar);
				final int thisPos = i;
				final CharTypes nextType = (nextChar != null) ? getCharType(nextChar) : null; 
				if (thisType.equals(nextType) || isRandomType(thisType,nextType)) {
					segment.append(thisChar);
				} else {	
					segment.append(thisChar);
					_segEnd = i;
					_currentUserSegment = null;
					if (thisType.equals(CharTypes.USER_ALPHA) || thisType.equals(CharTypes.USER_NUM)) {
						_userSegmentNumber++;
						_currentUserSegment = _userSegmentNumber-1;
					}
					newId.append(processCurrentSegment(segment.toString(), thisType, thisPos, templateInfo));
					segment.delete(0, segment.length());
					_segStart = i+1; 
				}
			}
			final String returnId = newId.toString();
			if (matchNumericOnly(projectId)) {
				if (!_numericList.contains(returnId.replaceAll("[^0-9]",""))) {
					return returnId;
				}
			} else if (!_subjectList.contains(returnId)) {
				return returnId;
			}
		}
		throw new SubjectIdsNonUniqueException("ERROR:  Could not compute a unique subject ID");
	}
	
	private boolean isRandomType(CharTypes thisType, CharTypes nextType) {
		return (thisType.equals(CharTypes.RANDOM) || thisType.equals(CharTypes.CONSTRAINED_RANDOM)) &&
				(nextType.equals(CharTypes.RANDOM) || nextType.equals(CharTypes.CONSTRAINED_RANDOM));
	}

	/**
	 * Checks if is subject ids plugin enabled.
	 *
	 * @param projectId the project id
	 * @return true, if is subject ids plugin enabled
	 */
	public boolean isSubjectIdsPluginEnabled(final String projectId) {
		try {
			return Boolean.valueOf(getProperties(projectId).getProperty(SubjectIdsConstants.SUBJECT_IDS_CONFIG_ENABLED));
		} catch (SubjectIdsConfigurationException e) {
			return false;
		}
	}

	public boolean matchNumericOnly(final String projectId) {
		try {
			return Boolean.valueOf(getProperties(projectId).getProperty(SubjectIdsConstants.SUBJECT_IDS_CONFIG_MATCHNUMERIC));
		} catch (SubjectIdsConfigurationException e) {
			return false;
		}
	}

	/**
	 * Returns the list of email recepients for when events
	 * must notifiy users.
	 * 
	 * @param projectId the project id
	 * @return List of email addresses.
	 */
	public List<String> notifierList(final String projectId) throws SubjectIdsConfigurationException {
		return Arrays.asList(getProperties(projectId).getProperty(SubjectIdsConstants.SUBJECT_IDS_CONFIG_NOTIFIERLIST).split(","));
	}
	
	/**
	 * Validate subject id.
	 *
	 * @param subjectId the subject id
	 * @param projectId the project id
	 * @return true, if successful
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 * @throws SubjectIdsFormatException the subject ids format exception
	 * @throws SubjectIdsNonUniqueException the subject ids non unique exception
	 */
	public boolean validateSubjectId(final String subjectId, final String projectId) throws SubjectIdsConfigurationException, SubjectIdsFormatException, SubjectIdsNonUniqueException {
		final ChecksumMethod csMethod = (getProjectChecksumMethod(projectId).equals("MOD9710")) ? ChecksumMethod.MOD9710 : ChecksumMethod.LASTDIGITS;
		final TemplateInfo templateInfo = getProjectTemplateInfo(projectId, false);
		return matchesTemplateInfo(subjectId, templateInfo) && validateChecksum(subjectId, projectId, csMethod, templateInfo);
	}
	
	/**
	 * Matches template.
	 *
	 * @param label the label
	 * @param template the template
	 * @return true, if successful
	 */
	private boolean matchesTemplateInfo(String label, TemplateInfo templateInfo) {
		final String template = templateInfo.getTemplate();
		if (!(label.length() == template.length())) {
			return false;
		}
		for (int i=0; i<label.length(); i++) {
			final char labelChar = label.charAt(i);
			final char templateChar = template.charAt(i);
			final CharTypes templateType = getCharType(templateChar);
			if (templateType.equals(CharTypes.LITERAL) && labelChar!=templateChar) {
				return false;
			} else if ((templateType.equals(CharTypes.USER_NUM) || templateType.equals(CharTypes.RANDOM)  || 
					templateType.equals(CharTypes.CONSTRAINED_RANDOM) || templateType.equals(CharTypes.SEQUENTIAL) ||
					templateType.equals(CharTypes.CHECKSUM)) && !CharUtils.isAsciiNumeric(labelChar)) {
			} else if (templateType.equals(CharTypes.CONSTRAINED_RANDOM) &&
					templateInfo.getConstraintRegex() != null &&
					templateInfo.getConstraintRegex().get(i) != null &&
					!String.valueOf(labelChar).matches(templateInfo.getConstraintRegex().get(i))) {
				return false;
			} else if (templateType.equals(CharTypes.USER_ALPHA) && !CharUtils.isAsciiAlpha(labelChar)) {
				return false;
			} else if (templateType.equals(CharTypes.USER_ALPHANUM) && !CharUtils.isAsciiAlphanumeric(labelChar)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validate checksum.
	 *
	 * @param subjectId the subject id
	 * @param projectId the project id
	 * @param csMethod the cs method
	 * @param template the template
	 * @return true, if successful
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 * @throws SubjectIdsFormatException the subject ids format exception
	 * @throws SubjectIdsNonUniqueException the subject ids non unique exception
	 */
	public boolean validateChecksum(final String subjectId, final String projectId, final ChecksumMethod csMethod, final TemplateInfo templateInfo) throws SubjectIdsConfigurationException, SubjectIdsFormatException, SubjectIdsNonUniqueException {
		_toBeChecksummed = new StringBuilder();
		_userSegmentNumber = 0;
		_segStart = 0;
		_segEnd = 0;
		_csMethod = csMethod;

		final String template = templateInfo.getTemplate();
		final StringBuilder segment = new StringBuilder();
		_segStart = 0; 
		for (int i=0; i<template.length(); i++) {
			final Character thisChar = template.charAt(i);
			final Character nextChar = (i != (template.length()-1)) ? template.charAt(i+1) : null; 
			final CharTypes thisType = getCharType(thisChar);
			final CharTypes nextType = (nextChar != null) ? getCharType(nextChar) : null; 
			if (thisType.equals(nextType)) {
				segment.append(thisChar);
			} else {	
				segment.append(thisChar);
				_segEnd = i;
				_currentUserSegment = null;
				if (thisType.equals(CharTypes.USER_NUM) || thisType.equals(CharTypes.RANDOM) || 
						thisType.equals(CharTypes.CONSTRAINED_RANDOM) || thisType.equals(CharTypes.SEQUENTIAL)) {
					_toBeChecksummed.append(subjectId.substring(_segStart, _segEnd+1));
					_userSegmentNumber++;
					_currentUserSegment = _userSegmentNumber-1;
				}
				if (thisType.equals(CharTypes.CHECKSUM)) {
					switch(_csMethod) {
						case MOD9710: return validateMod9710Checksum(_toBeChecksummed.toString(), subjectId.substring(_segStart, _segEnd+1)); 
						case LASTDIGITS: return validateLastDigitsChecksum(_toBeChecksummed.toString(), subjectId.substring(_segStart, _segEnd+1)); 
						default: return false;
					}
				}
				segment.delete(0, segment.length());
				_segStart = i+1; 
			}
		}
		// Let's call it valid if there's no checksum component
		return true;
	}

	/**
	 * Process current segment.
	 *
	 * @param segment the segment
	 * @param segType the seg type
	 * @return the string
	 * @throws SubjectIdsFormatException the subject ids format exception
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 */
	private String processCurrentSegment(String segment, CharTypes segType, int segPos, TemplateInfo templateInfo) throws SubjectIdsFormatException, SubjectIdsConfigurationException {
		switch(segType) {
			case LITERAL: return segment; 
			case USER_ALPHA: return processUserAlphaSegment(segment); 
			case USER_NUM: return processUserNumericSegment(segment);  
			case USER_ALPHANUM: return processUserAlphanumericSegment(segment); 
			case SEQUENTIAL: return processSequentialSegment(segment); 
			case RANDOM: return processRandomSegment(segment, segPos, templateInfo.getConstraintRegex());
			case CHECKSUM: return processChecksumSegment(segment); 
			default: return null;
		}
	}

	/**
	 * Process user segment.
	 *
	 * @param segment the segment
	 * @param isAlpha the is alpha
	 * @param isNumeric the is numeric
	 * @return the string
	 * @throws SubjectIdsFormatException the subject ids format exception
	 */
	private String processUserSegment(String segment, boolean isAlpha, boolean isNumeric) throws SubjectIdsFormatException {
		if (_userSuppliedBits.length<_currentUserSegment+1) {
			throw new SubjectIdsFormatException("Insufficient user-supplied segments to populate ID");
		}
		final String userSegment = _userSuppliedBits[_currentUserSegment];
		if (userSegment.length()!=segment.length()) {
			throw new SubjectIdsFormatException("Length of user-supplied segment does not match template specification (" + 
					segment + " expected to be length " + userSegment.length() + ")." );
		}
		if (isAlpha && !StringUtils.isAlpha(userSegment)) {
			throw new SubjectIdsFormatException("User-supplied segment does not match template specification (" + 
					segment + " expected to contain only letters)." );
		}
		if (isNumeric && !StringUtils.isNumeric(userSegment)) {
			throw new SubjectIdsFormatException("User-supplied segment does not match template specification (" + 
					segment + " expected to contain only numbers)." );
		}
		return userSegment;
	}
	
	/**
	 * Process user alphanumeric segment.
	 *
	 * @param segment the segment
	 * @return the string
	 * @throws SubjectIdsFormatException the subject ids format exception
	 */
	private String processUserAlphanumericSegment(String segment) throws SubjectIdsFormatException {
		return processUserSegment(segment, true, false);
	}
	
	/**
	 * Process user alpha segment.
	 *
	 * @param segment the segment
	 * @return the string
	 * @throws SubjectIdsFormatException the subject ids format exception
	 */
	private String processUserAlphaSegment(String segment) throws SubjectIdsFormatException {
		return processUserSegment(segment, true, false);
	}
	
	/**
	 * Process user numeric segment.
	 *
	 * @param segment the segment
	 * @return the string
	 * @throws SubjectIdsFormatException the subject ids format exception
	 */
	private String processUserNumericSegment(String segment) throws SubjectIdsFormatException {
		final String userSeg = processUserSegment(segment, false, true);
		_toBeChecksummed.append(userSeg);
		return userSeg;
	}
	
	/**
	 * Process checksum segment.
	 *
	 * @param segment the segment
	 * @return the string
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 */
	private String processChecksumSegment(String segment) throws SubjectIdsConfigurationException {
		final String csSegment = _toBeChecksummed.toString();
		if (segment.length()!=2 && _csMethod.equals(ChecksumMethod.MOD9710)) {
			throw new SubjectIdsConfigurationException("ERROR:  The ISO 7064 Mod 97,10 checksum method requires specification of a two digit checksum");
		}
		if (csSegment == null || csSegment.length()<1 ) {
			throw new SubjectIdsConfigurationException("ERROR:  There are no values upon which to calculate a checksum");
		}
		switch(_csMethod) {
			case MOD9710: return calculateMod9710Checksum(csSegment); 
			case LASTDIGITS: return calculateLastDigitsChecksum(csSegment, segment); 
			default: return null;
		}
	}

	/**
	 * Calculate last digits checksum.
	 *
	 * @param csSegment the cs segment
	 * @param templSegment the templ segment
	 * @return the string
	 */
	private String calculateLastDigitsChecksum(String csSegment, String templSegment) {
		long value = 0;
		for (int i = 0; i<csSegment.length(); i++) {
			final int multiplier = csSegment.length()-i;
			value = value + (Long.valueOf(String.valueOf(csSegment.charAt(i)))*multiplier);
		}
		String strValue = String.valueOf(value);
		if (strValue.length()<templSegment.length()) {
			final int multiplier = templSegment.length()-strValue.length();
			for (int i=1; i<=multiplier; i++) {
				strValue = "0" + strValue;
			}
		}
		return strValue.substring(strValue.length()-templSegment.length());
	}

	/**
	 * Validate last digits checksum.
	 *
	 * @param segment the segment
	 * @param checksum the checksum
	 * @return true, if successful
	 */
	private boolean validateLastDigitsChecksum(String segment, String checksum) {
		return calculateLastDigitsChecksum(segment, checksum).equals(checksum);
	}

	/**
	 * Calculate mod9710 checksum.
	 *
	 * @param csSegment the cs segment
	 * @return the string
	 */
	private String calculateMod9710Checksum(String csSegment) {
		final long value = (98-((Long.valueOf(csSegment)*100) % 97)) % 97;
		final String stringValue = Long.toString(value);
		return (stringValue.length()>1) ? stringValue : "0" + stringValue;
	}

	/**
	 * Validate mod9710 checksum.
	 *
	 * @param segment the segment
	 * @param checksum the checksum
	 * @return true, if successful
	 */
	private boolean validateMod9710Checksum(String segment, String checksum) {
		final long value = Long.valueOf(segment+checksum) % 97;
		return value==1;
	}

	/**
	 * Process random segment.
	 *
	 * @param segment the segment
	 * @param arrayList 
	 * @param segPos 
	 * @return the string
	 */
	private String processRandomSegment(String segment, int segPos, ArrayList<String> arrayList) {
		// We'll try to pull a unique value for this segment
		final List<String> currList = Lists.newArrayList();
		for (String id : _subjectList) {
			final String subs = id.substring(_segStart, _segEnd+1);
			currList.add(subs);
		}
		String ranSeg = null;
		for (int k=1; k<currList.size() || k==1; k++) {
			final StringBuilder sb = new StringBuilder();
			for (int i=0; i<segment.length(); i++) {
				Integer ranInt = random.nextInt(10);
				final String regEx = arrayList.get(segPos-segment.length()+1+i);
				while (regEx != null && !ranInt.toString().matches("^" + regEx + "$")) {
					ranInt = random.nextInt(10);
				}
				sb.append(ranInt);
			}
			ranSeg = sb.toString();
			if (!currList.contains(ranSeg)) {
				_toBeChecksummed.append(ranSeg);
				return ranSeg;
			}
		}
		_toBeChecksummed.append(ranSeg);
		return ranSeg;
	}

	/**
	 * Process sequential segment.
	 *
	 * @param segment the segment
	 * @return the string
	 */
	private String processSequentialSegment(String segment) {
		long currentMax = 0;
		for (String id : _subjectList) {
			final String subs = id.substring(_segStart, _segEnd+1);
			try {
				final long currentVal = Long.valueOf(subs);
				currentMax = (currentVal > currentMax) ? currentVal : currentMax;
			} catch (Exception e) {
				// Do nothing
			}
		}
		String returnVal = String.valueOf(currentMax+1);
		if (returnVal.length()<segment.length()) {
			final int multiplier = segment.length()-returnVal.length();
			for (int i=1; i<=multiplier; i++) {
				returnVal = "0" + returnVal;
			}
		}
		_toBeChecksummed.append(returnVal);
		return returnVal;
	}
	
	/**
	 * Gets the subject list.
	 *
	 * @param projectId the project id
	 * @param user the user
	 * @param template the template
	 * @return the subject list
	 */
	private List<String> getSubjectList(String projectId, TemplateInfo templateInfo) { 
		try {
			// NEED admin user here
			@SuppressWarnings("deprecation")
			final UserI adminUser = AdminUtils.getAdminUser();
			final String projectList = getProperties(projectId).getProperty(SubjectIdsConstants.SUBJECT_IDS_CONFIG_PROJECTLIST);
			final QueryOrganizer qo = new QueryOrganizer("xnat:subjectData", adminUser,
					ViewManager.ALL);
			
			qo.addField("xnat:subjectdata/project");
			qo.addField("xnat:subjectdata/label");
			
			final CriteriaCollection cc= new CriteriaCollection("OR");
			cc.addClause("xnat:subjectData/project", projectId);
			cc.addClause("xnat:subjectData/sharing/share/project", projectId);
			if (projectList!=null && projectList.length()>0) {
				for (final String ckProj : projectList.split(",")) {
					if (ckProj != null && ckProj.length()>0 && !ckProj.equals(projectId)) {
						cc.addClause("xnat:subjectData/project", ckProj);
						cc.addClause("xnat:subjectData/sharing/share/project", ckProj);
					}
				}
			}
			qo.setWhere(cc);

			final String query = qo.buildQuery();

			final XFTTable table = XFTTable.Execute(query, adminUser.getDBName(), adminUser.getLogin());
			table.setColumns(new String[] {"ID", "project", "label"});

			final Integer labelI=table.getColumnIndex("label");
			final Integer idI=table.getColumnIndex("ID");
			if(labelI!=null && idI!=null){
				final XFTTable t= XFTTable.Execute("SELECT subject_id,label FROM xnat_projectParticipant WHERE project='"+ projectId + "'", adminUser.getDBName(), adminUser.getUsername());
				@SuppressWarnings("rawtypes")
				final Hashtable lbls=t.toHashtable("subject_id", "label");
				for(final Object[] row:table.rows()){
					final String id=(String)row[idI];
					if(lbls.containsKey(id)){
						final String lbl=(String)lbls.get(id);
						if(null!=lbl && !lbl.equals("")){
							row[labelI]=lbl;
						}
					}
				}
			}
			final List<String> returnList = Lists.newArrayList();
			for (final Object o : table.convertColumnToArrayList("label")) {
				final String label = o.toString();
				if (matchesTemplateInfo(label, templateInfo) || matchNumericOnly(projectId)) {
					returnList.add(label);
				}
			}
			Collections.sort(returnList);
			return returnList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	/**
	 * Gets the char type.
	 *
	 * @param thisChar the this char
	 * @return the char type
	 */
	private CharTypes getCharType(char thisChar) {
		switch(thisChar) {
			case '@': return CharTypes.USER_ALPHA; 
			case '$': return CharTypes.USER_ALPHANUM; 
			case '#': return CharTypes.USER_NUM; 
			case '!': return CharTypes.RANDOM; 
			case '^': return CharTypes.CONSTRAINED_RANDOM; 
			case '%': return CharTypes.SEQUENTIAL; 
			case '?': return CharTypes.CHECKSUM; 
			default: return CharTypes.LITERAL;
		}
	}
	
	/**
	 * Gets the project template.
	 *
	 * @param projectId the project id
	 * @return the project template
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 */
	private TemplateInfo getProjectTemplateInfo(final String projectId, boolean newTemplate) throws SubjectIdsConfigurationException {
		String origTemplate = null;
		if (newTemplate) {
			origTemplate = getProperties(projectId).getProperty(SubjectIdsConstants.SUBJECT_IDS_CONFIG_NEWTEMPLATE);
		}
		if (origTemplate == null || origTemplate.trim().length() == 0) {
			origTemplate = getProperties(projectId).getProperty(SubjectIdsConstants.SUBJECT_IDS_CONFIG_TEMPLATE);
		}
		final StringBuilder templateBuilder = new StringBuilder();
		final ArrayList<String> constraintList = new ArrayList<String>();
		StringBuilder constraintBuilder = new StringBuilder();
		boolean inConstraint = false;
		int constraintPos = 0;
		for (int i=0; i<origTemplate.length(); i++) {
			final String istr = origTemplate.substring(i,i+1);
			if (!(inConstraint || istr.equals("{") || istr.equals("}"))) {
				templateBuilder.append(istr);
				constraintList.add(null);
				constraintPos++;
			} else {
				if (istr.equals("{")) {
					constraintPos=i;
					constraintBuilder.append("[");
					inConstraint = true;
				} else if (istr.equals("}")) {
					templateBuilder.append("^");
					constraintBuilder.append("]");
					constraintList.add(constraintPos,constraintBuilder.toString());
					constraintBuilder = new StringBuilder();
					constraintPos++;
					inConstraint = false;
				} else if (inConstraint) {
					constraintBuilder.append(istr);
				}
			}
		}
		return new TemplateInfo(templateBuilder.toString(),constraintList);
	}

	/**
	 * Gets the project checksum method.
	 *
	 * @param projectId the project id
	 * @return the project checksum method
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 */
	private String getProjectChecksumMethod(final String projectId) throws SubjectIdsConfigurationException {
		return getProperties(projectId).getProperty(SubjectIdsConstants.SUBJECT_IDS_CONFIG_CHECKSUM);
	}
	
	/**
	 * Gets the properties.
	 *
	 * @param projectId the project id
	 * @return the properties
	 * @throws SubjectIdsConfigurationException the subject ids configuration exception
	 */
	private Properties getProperties(final String projectId) throws SubjectIdsConfigurationException {
		final Properties props = _prefService.getToolProperties(SubjectIdsConstants.SUBJECT_IDS_TOOL_ID, Scope.Project, projectId);	
		if (props == null || !props.containsKey(SubjectIdsConstants.SUBJECT_IDS_CONFIG_ENABLED) ||
				!props.containsKey(SubjectIdsConstants.SUBJECT_IDS_CONFIG_TEMPLATE)) {
			throw new SubjectIdsConfigurationException("ERROR:  Coult not read stored configuration properties"); 
		}
		return props;
	}

	public String labelDuplicatedInProject(XnatExperimentdata experiment) {
			// NEED admin user here
			@SuppressWarnings("deprecation")
			final UserI adminUser = AdminUtils.getAdminUser();
			final String projectId = experiment.getProject();
			final String expLabel = experiment.getLabel();
			String projectListConfig;
			try {
				projectListConfig = getProperties(projectId).getProperty(SubjectIdsConstants.SUBJECT_IDS_CONFIG_PROJECTLIST);
			} catch (SubjectIdsConfigurationException e) {
				return null;
			}
			if (projectListConfig==null || projectListConfig.trim().length()<1) {
				return null;
			}
			List<String> projectList = Arrays.asList(projectListConfig.split(","));
	    	final CriteriaCollection cc=new CriteriaCollection("OR");
	        cc.addClause("xnat:experimentData/label", expLabel);
			final List<XnatExperimentdata> expList = XnatExperimentdata.getXnatExperimentdatasByField(cc, adminUser, true);
			for (final XnatExperimentdata exp : expList) {
				final String compProject = exp.getProject();
				if (!compProject.equals(projectId) && projectList.contains(compProject)) {
					return compProject;
				}
			}
			return null;
	}

}
