package org.nrg.ccf.subjectids.xapi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;
import java.util.Properties;


import org.nrg.ccf.subjectids.exception.SubjectIdsConfigurationException;
import org.nrg.ccf.subjectids.exception.SubjectIdsFormatException;
import org.nrg.ccf.subjectids.utils.SubjectIdsUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * The Class CCFSubjectIdsController.
 *
 * @author Mike Hodge
 */

@XapiRestController
@Api(description = "CCF Subject Ids Configuration API")
public class CCFSubjectIdsController extends AbstractXapiRestController {
	
	/** The _id utils. */
	final SubjectIdsUtils _idUtils;
	
	/** The Constant PROPERTIES_EXCLUSIONS. */
	public static final String[] PROPERTIES_EXCLUSIONS = new String[] { "XNAT_CSRF" };
	//private static final Logger _logger = LoggerFactory.getLogger(CCFSubjectIdsController.class);
	
	/**
	 * Instantiates a new CCF subject ids controller.
	 *
	 * @param idUtils the id utils
	 * @param userManagementService the user management service
	 * @param roleHolder the role holder
	 */
	@Autowired
	public CCFSubjectIdsController(final SubjectIdsUtils idUtils, UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
		this._idUtils = idUtils;
	}
	
	/**
	 * Gets the new id.
	 *
	 * @param projectId the project id
	 * @param userSuppliedSegments the user supplied segments
	 * @return the new id
	 */
	@ApiOperation(value = "Returns new SubjectID for project",  response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Subject ID successfully returned."),  @ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/ccfSubjectIds/getNewId", produces = MediaType.APPLICATION_JSON_VALUE,
						consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<Properties> getNewId(@PathVariable("projectId") String projectId , @RequestBody List<String> userSuppliedSegments) {
		try {
			final UserI user = getSessionUser();
			final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
			if (project == null) {
				return new ResponseEntity<>(statusProperties("ERROR:  Project not found"), HttpStatus.BAD_REQUEST);
			}else {
				projectId = project.getId();
			}
			if (!project.canCreate(user)) {
				return new ResponseEntity<>(statusProperties("ERROR:  User must have write access to project to generate subject IDs."), HttpStatus.UNAUTHORIZED);
			}
			final String newId = _idUtils.getNewSubjectId(user, projectId, userSuppliedSegments.toArray(new String[userSuppliedSegments.size()]));
			return new ResponseEntity<>(resultProperties(newId), HttpStatus.OK);
		} catch (SubjectIdsConfigurationException e) {
			return new ResponseEntity<>(statusProperties("ERROR:  Configuration error (" +  e + ")"),HttpStatus.INTERNAL_SERVER_ERROR );
		} catch (SubjectIdsFormatException e) {
			return new ResponseEntity<>(statusProperties("ERROR:  Format error (" +  e + ")"),HttpStatus.INTERNAL_SERVER_ERROR );
		} catch (Exception e) {
			return new ResponseEntity<>(statusProperties("ERROR:  Unexpected server error (" +  e + ")"),HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	/**
	 * Gets the new id.
	 *
	 * @param projectId the project id
	 * @param subjectId the subject id
	 * @return the new id
	 */
	@ApiOperation(value = "Validates SubjectID for project",  response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Subject ID validation successful."),  @ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/ccfSubjectIds/validateId/{subjectId}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<Properties> getNewId(@PathVariable("projectId") String projectId, @PathVariable("subjectId") String subjectId) {
		try {
			final UserI user = getSessionUser();
			final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
			if (project == null) {
				return new ResponseEntity<>(statusProperties("ERROR:  Project not found"), HttpStatus.BAD_REQUEST);
			}else {
				projectId = project.getId();
			}
			if (!project.canRead(user)) {
				return new ResponseEntity<>(statusProperties("ERROR:  User must have read access to project to validate subject IDs."), HttpStatus.UNAUTHORIZED);
			}
			boolean isValid = _idUtils.validateSubjectId(subjectId, projectId);
			Properties p = new Properties();
			p.setProperty("isValid", String.valueOf(isValid));
			return new ResponseEntity<>(p, HttpStatus.OK);
			
		} catch (SubjectIdsConfigurationException e) {
			return new ResponseEntity<>(statusProperties("ERROR:  Configuration error (" +  e + ")"),HttpStatus.INTERNAL_SERVER_ERROR );
		} catch (SubjectIdsFormatException e) {
			return new ResponseEntity<>(statusProperties("ERROR:  Format error (" +  e + ")"),HttpStatus.INTERNAL_SERVER_ERROR );
		} catch (Exception e) {
			return new ResponseEntity<>(statusProperties("ERROR:  Unexpected server error (" +  e + ")"),HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	/**
	 * Status properties.
	 *
	 * @param status the status
	 * @return the properties
	 */
	private Properties statusProperties(String status) {
		final String STATUS = "error_status";
		final Properties props = new Properties();
		props.setProperty(STATUS, status);
		return props;
	}
	
	/**
	 * Result properties.
	 *
	 * @param result the result
	 * @return the properties
	 */
	private Properties resultProperties(String result) {
		final String RESULT = "newSubjectId";
		final Properties props = new Properties();
		props.setProperty(RESULT, result);
		return props;
	}
	
}
