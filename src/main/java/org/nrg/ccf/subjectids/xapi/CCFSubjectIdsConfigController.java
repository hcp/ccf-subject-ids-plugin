package org.nrg.ccf.subjectids.xapi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.nrg.ccf.subjectids.utils.SubjectIdsConstants;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.constants.Scope;
import org.nrg.prefs.entities.Tool;
import org.nrg.prefs.services.NrgPreferenceService;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.collect.Maps;

/**
 * The Class CCFSubjectIdsConfigController.
 *
 * @author Mike Hodge
 */

@XapiRestController
@Api(description = "CCF Subject Ids Configuration API")
public class CCFSubjectIdsConfigController extends AbstractXapiRestController {
	
	/** The _pref service. */
	final NrgPreferenceService _prefService;

	/** The Constant _logger. */
	@SuppressWarnings("unused")
	private static final Logger _logger = LoggerFactory.getLogger(CCFSubjectIdsConfigController.class);
	
	/**
	 * Instantiates a new CCF subject ids config controller.
	 *
	 * @param prefService the pref service
	 * @param userManagementService the user management service
	 * @param roleHolder the role holder
	 */
	@Autowired
	public CCFSubjectIdsConfigController(final NrgPreferenceService prefService, UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
		this._prefService = prefService;
	}
	
	/**
	 * Post-construct initializer
	 */
	@PostConstruct
	public void initIt() {
		if (!_prefService.getToolIds().contains(SubjectIdsConstants.SUBJECT_IDS_TOOL_ID)) { 
			Tool tool = new Tool();
			tool.setToolId(SubjectIdsConstants.SUBJECT_IDS_TOOL_ID);
			tool.setToolName(SubjectIdsConstants.SUBJECT_IDS_TOOL_NAME);
			tool.setToolDescription(SubjectIdsConstants.SUBJECT_IDS_TOOL_DESC);
			tool.setStrict(false);
			_prefService.createTool(tool);
		}
	}

	/**
	 * Gets the configuration.
	 *
	 * @param projectId the project id
	 * @return the configuration
	 */
	@ApiOperation(value = "Returns SubjectIDs configuration for project", response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Subject IDs configuration successfully updated."),  @ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/ccfSubjectIds/config", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<Properties> getConfiguration(@PathVariable("projectId") String projectId) {
		//curl -H "Content-Type: application/json" -X POST -d '{  "project":"TEST1ID",  "sync_frequency":"dily",  "auto_sync":"false",  "identifiers":"use_local",  "remote_url":"http://localhost:8080/xnat",  "remote_project_id":"SyncProjectId"}' -u admin  "http://localhost:8080/xnat/xapi/xsync/setup?project=TEST1ID"
		try {
			final UserI user = getSessionUser();
			final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
			if (project == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}else {
				projectId = project.getId();
			}
			if (!project.canRead(user)) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				
			}
			final Properties props = _prefService.getToolProperties(SubjectIdsConstants.SUBJECT_IDS_TOOL_ID, Scope.Project, projectId);
			return new ResponseEntity<>(props,  HttpStatus.OK);
		}catch (Exception  exception) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	/**
	 * Do configuration.
	 *
	 * @param projectId the project id
	 * @param bodyProperties the body properties
	 * @return the response entity
	 */
	@ApiOperation(value = "Configures SubjectIDs template for project", response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Subject IDs configuration successfully updated."),  @ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/ccfSubjectIds/config", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> doConfiguration(@PathVariable("projectId") String projectId, @RequestBody Properties bodyProperties) {
		// NOTE:  JSON returns 415 when RequestBody is specified as a Map, while FORM data returns a 415 when RequestBody is specified as Properties.  Hence, two methods.
		final Map<String,String> bodyMap = Maps.newHashMap();
		for (final String name: bodyProperties.stringPropertyNames()) {
			bodyMap.put(name, bodyProperties.getProperty(name));
		}
		return returnConfigRequest(projectId, bodyMap);
	}
	
	/**
	 * Do configuration.
	 *
	 * @param projectId the project id
	 * @param bodyMap the body map
	 * @return the response entity
	 */
	@ApiOperation(value = "Configures SubjectIDs template for project", response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Subject IDs configuration successfully updated."),  @ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/ccfSubjectIds/config", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<String> doConfiguration(@PathVariable("projectId") String projectId, @RequestBody MultiValueMap<String, String> bodyMap) {
		// NOTE:  JSON returns 415 when RequestBody is specified as a Map, while FORM data returns a 415 when RequestBody is specified as Properties.  Hence, two methods.
		// NOTE:  2016/09/28.  APPLICATION_FORM_URLENCODED_VALUE requests no longer seem to be working.  Not sure why.  Changed javascript to make JSON call.  This
		// method is currently not used.
		return returnConfigRequest(projectId, bodyMap.toSingleValueMap());
	}
	
	/**
	 * Return config request.
	 *
	 * @param projectId the project id
	 * @param bodyMap the body map
	 * @return the response entity
	 */
	private ResponseEntity<String> returnConfigRequest(String projectId, Map<String, String> bodyMap) {
		try {
			//
			final UserI user = getSessionUser();
			final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
			if (project == null) {
				return new ResponseEntity<>("Project not provided or is invalid",HttpStatus.BAD_REQUEST);
			}else {
				projectId = project.getId();
			}
			if (!project.canDelete(user)) {
				return new ResponseEntity<>("User is not authorized to update this configuration.",HttpStatus.UNAUTHORIZED);
			}
			for (final Entry<String, String> entry : bodyMap.entrySet()) {
				if (Arrays.asList(SubjectIdsConstants.PROPERTIES_EXCLUSIONS).contains(entry.getKey())) {
					continue;
				}
				if (!_prefService.hasPreference(SubjectIdsConstants.SUBJECT_IDS_TOOL_ID, entry.getKey(), Scope.Project, projectId)) {
					_prefService.create(SubjectIdsConstants.SUBJECT_IDS_TOOL_ID, entry.getKey(), Scope.Project, projectId, entry.getValue());
				} 
				_prefService.setPreferenceValue(SubjectIdsConstants.SUBJECT_IDS_TOOL_ID, entry.getKey(), Scope.Project, projectId, entry.getValue());
			}
			return new ResponseEntity<>(projectId + " SubjectIDs configuration saved",  HttpStatus.OK);
		}catch (Exception  exception) {
			return new ResponseEntity<>(projectId + " Xsync Setup failed ", HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}

}
