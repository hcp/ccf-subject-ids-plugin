package org.nrg.ccf.subjectids.event.listeners;

import org.nrg.ccf.common.utilities.components.QueryUtils;
import org.nrg.xdat.turbine.utils.AdminUtils;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.fn.Consumer;
import org.nrg.ccf.subjectids.exception.SubjectIdsConfigurationException;
import org.nrg.ccf.subjectids.exception.SubjectIdsFormatException;
import org.nrg.ccf.subjectids.exception.SubjectIdsNonUniqueException;
import org.nrg.ccf.subjectids.utils.SubjectIdsUtils;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.BaseXnatProjectdata;
import org.nrg.xft.event.entities.WorkflowStatusEvent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.utils.WorkflowUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

import static reactor.bus.selector.Selectors.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;


/**
 * The Class InvalidSubjectIdNotifier.
 */
@Slf4j
@Service
public class InvalidIdNotifier implements Consumer<Event<WorkflowStatusEvent>> {
	
	/** The _id utils. */
	private final SubjectIdsUtils _idUtils;
	private final ConfigService _configService;
	private final String TOOL_NAME = "subjectIdsSessionLabelValidation";
	private final String CONFIG_PATH = "configuration";
	private final String XSYNC_PIPELINE_VALUE = "Xsync";
	private final String SYNCED_AND_NOT_VERIFIED_VALUE = "SYNCED_AND_NOT_VERIFIED";
	private final String FAILED_VALUE = "Failed";
	private final Gson _gson = new Gson();
	private static final Logger _logger = LoggerFactory.getLogger(InvalidIdNotifier.class);
	private ArrayList<String> _enabledUserEmailList = new  ArrayList<>();
	private enum EmailType { INVALID_EXPERIMENT, DUPLICATE_EXPERIMENT_LABEL, XSYNC_NOT_VERIFIED, XSYNC_FAILED };

    /**
     * Instantiates a new invalid subject id notifier.
     *
     * @param eventBus the event bus
     * @param idUtils the id utils
     */
    @Inject
    public InvalidIdNotifier(EventBus eventBus, final SubjectIdsUtils idUtils, final ConfigService configService, 
    		QueryUtils queryUtils) {
        _idUtils = idUtils;
        _configService = configService;
        _enabledUserEmailList.addAll(queryUtils.getEnabledUserEmailList());
        eventBus.on(R(WorkflowStatusEvent.class.getName() + "[.]?(" + PersistentWorkflowUtils.COMPLETE + "|" + PersistentWorkflowUtils.FAILED +
        		"|" + this.SYNCED_AND_NOT_VERIFIED_VALUE + ")"), this);
    }

    /* (non-Javadoc)
     * @see reactor.fn.Consumer#accept(java.lang.Object)
     */
    @Override
    public void accept(Event<WorkflowStatusEvent> event) {

        final WorkflowStatusEvent wfsEvent = event.getData();
        if (wfsEvent.getWorkflow() instanceof WrkWorkflowdata) {
            handleEvent(wfsEvent);
        }

    }

	/**
	 * Handle event.
	 * @param wfsEvent the wfs event
	 */
	private void handleEvent(WorkflowStatusEvent wfsEvent) {
		final WrkWorkflowdata workflow = (WrkWorkflowdata)wfsEvent.getWorkflow();
		final UserI user = workflow.getUser();
		final String pipeline = s_getNormalPipeline(workflow);
		final String projectId = workflow.getExternalid();

		if (workflow.getStatus().equals(PersistentWorkflowUtils.COMPLETE)) {
			if (s_isSubjectWorkflow(workflow) && s_isValidSubjectPipeline(pipeline)) {
				// Handle sending email related to invalid Subject.
				// We really only want to catch subject creation/renaming types of workflows here.
				m_sendInvalidSubjectEmail(user, projectId, workflow);
			} else if (s_isValidExperimentPipeline(pipeline)) {
				// Handle sending email related to invalid Experiment.
				// We really only want to catch subject creation/renaming types of workflows here.
				m_sendExperimentEmail(user, projectId, workflow, EmailType.INVALID_EXPERIMENT);
				// Do duplicate experiment check
				m_sendExperimentEmail(user, projectId, workflow, EmailType.DUPLICATE_EXPERIMENT_LABEL);
			}
			// If we get an XSync complete workflow, let's clean up any failed/not_verified workflows
			if (s_getNormalPipeline(workflow).equalsIgnoreCase(XSYNC_PIPELINE_VALUE)) {
				final CriteriaCollection cc= new CriteriaCollection("AND");
				cc.addClause("wrk:workFlowData.ID",workflow.getId());
				final List<WrkWorkflowdata> workflows = WrkWorkflowdata.getWrkWorkflowdatasByField(cc, user, false);
				for (final WrkWorkflowdata wf : workflows) {
					if (wf.equals(workflow)) {
						continue;
					}
					if (s_getNormalPipeline(wf).equalsIgnoreCase(XSYNC_PIPELINE_VALUE) && (
							wf.getStatus().equalsIgnoreCase(SYNCED_AND_NOT_VERIFIED_VALUE) ||
							wf.getStatus().equalsIgnoreCase(FAILED_VALUE))
							)  {
						wf.setStatus("Failed (Dismissed)");
						try {
							WorkflowUtils.save(wf, wf.buildEvent());
						} catch (Exception e) {
							log.error("ERROR:  Couldn't save workflow" + e.toString());
						}
					}
				}

			}
			
		} else {
			// Latch onto this notifier to catch SYNCED_AND_NOT_VERIFIED workflows and notify
			if (s_getNormalPipeline(workflow).equalsIgnoreCase(XSYNC_PIPELINE_VALUE)) {
				if (workflow.getStatus().equalsIgnoreCase(SYNCED_AND_NOT_VERIFIED_VALUE)) {
					m_sendExperimentEmail(user, projectId, workflow, EmailType.XSYNC_NOT_VERIFIED);
				} else if (workflow.getStatus().equalsIgnoreCase(FAILED_VALUE)) {
					m_sendExperimentEmail(user, projectId, workflow, EmailType.XSYNC_FAILED);
				}
			}
		}
	}

	private boolean isDuplicateExperimentId(String projectId, WrkWorkflowdata workflow) {
		// TODO Auto-generated method stub
		return false;
	}

	private void m_sendInvalidSubjectEmail(UserI user, final String projectId, WrkWorkflowdata workflow)
	{
		if (!_idUtils.isSubjectIdsPluginEnabled(projectId)) {
			return;
		}

		final ArrayList<String> notifierList = m_getEmailList(user, projectId);
		final XnatSubjectdata subject = XnatSubjectdata.getXnatSubjectdatasById(workflow.getId(), user, false);
		if (subject == null) {
			return;
		}

		final String subjectLabel = subject.getLabel();

		if (!m_isInvalidSubjectId(subjectLabel, projectId)) {
			return;
		}

		// Bail if none to notify
		if (notifierList.size() < 1) {
			return;
		}

		final String eMessage =
			"The subject ID <b>" + subjectLabel + "</b> is invalid " +
			"for project <b>" + projectId + "</b>. &nbsp;It either " +
			"doesn't match the template specified for the project " +
			"or it fails a checksum check. &nbsp;Please verify that " +
			"this subject wasn't entered incorrectly.";
		final String eSubject =
			XDAT.getSiteConfigPreferences().getSiteId() +
			": Invalid subject ID for project " + projectId + " - " +
			subjectLabel;
		s_sendEmail(eSubject, eMessage, notifierList);
	}

	private void m_sendExperimentEmail(UserI user, final String projectId, WrkWorkflowdata workflow, EmailType emailType)
	{
		List<Map<String, String>> configMapList = m_getConfigMap(workflow);
		for (final Map<String, String> configMap : configMapList) {
			if (!(configMap.containsKey("xsiType") && configMap.containsKey("regex"))) {
				continue;
			}

			final String xsiType = configMap.get("xsiType");
			String regex = configMap.get("regex");
			if (!workflow.getDataType().equals(xsiType)) {
				continue;
			}

			final XnatExperimentdata experiment = XnatExperimentdata.getXnatExperimentdatasById(workflow.getId(), user, false);
			final String experimentLabel = experiment.getLabel();
			if (regex.contains("SUBJECTID") && experiment instanceof XnatSubjectassessordata) {
				final XnatSubjectdata subject = ((XnatSubjectassessordata)experiment).getSubjectData();
				regex = regex.replaceAll("SUBJECTID", subject.getLabel());
			}

			if (emailType.equals(EmailType.INVALID_EXPERIMENT) && experimentLabel.matches(regex)) {
				continue;
			}

			ArrayList<String> notifierList = m_getEmailList(user, projectId);


			String eMessage = null;
			String eSubject = null;
			if (emailType.equals(EmailType.INVALID_EXPERIMENT)) {
				eMessage = "The experiment ID <b>" + experimentLabel + "</b> is " +
						"invalid for project <b>" + projectId + "</b>. Please check " +
						"that this experiment ID wasn't entered incorrectly";
				eSubject =
						XDAT.getSiteConfigPreferences().getSiteId() + ": Invalid " +
								"experiment ID for project " + projectId + " - " +
								experimentLabel;
			} else if (emailType.equals(EmailType.DUPLICATE_EXPERIMENT_LABEL)) {
				final String duplicateLabelProject = _idUtils.labelDuplicatedInProject(experiment);
				if (duplicateLabelProject==null) {
					continue;
				}
				eMessage = "The experiment ID <b>" + experimentLabel + "</b> already " +
						"exists in project <b>" + duplicateLabelProject + "</b>. Please verify " +
						"that this is okay.";
				eSubject =
						XDAT.getSiteConfigPreferences().getSiteId() + ": Duplicate " +
								"experiment ID for project " + projectId + " - " +
								experimentLabel;
			} else if (emailType.equals(EmailType.XSYNC_NOT_VERIFIED)) {
				eMessage = "The XSync status for experiment ID <b>" + experimentLabel + "</b> is " +
						this.SYNCED_AND_NOT_VERIFIED_VALUE + ".  Please resend session " +
						"or verify that it is complete";
				eSubject =
						XDAT.getSiteConfigPreferences().getSiteId() + ": SYNCED_AND_NOT_VERIFIED " +
								"for project " + projectId + " , session " +
								experimentLabel;
			} else if (emailType.equals(EmailType.XSYNC_FAILED)) {
				eMessage = "The XSync status for experiment ID <b>" + experimentLabel + "</b> is " +
						this.FAILED_VALUE + ".  Please resend session " +
						"or verify that it is complete";
				eSubject =
						XDAT.getSiteConfigPreferences().getSiteId() + ": XSync failed " +
								"for project " + projectId + " , session " +
								experimentLabel;
			}
			if (eSubject != null && eMessage != null && notifierList != null) {
				s_sendEmail(eSubject, eMessage, notifierList);
			}
		}
	}

	private static String s_getNormalPipeline(WrkWorkflowdata workflow) {
		return workflow.getPipelineName().toLowerCase();
	}

	private static boolean s_isSubjectWorkflow(WrkWorkflowdata workflow) {
		return workflow.getDataType().equals(XnatSubjectdata.SCHEMA_ELEMENT_NAME);
	}

	private static boolean s_isValidSubjectPipeline(String pipeline) {
		return !(
			pipeline.contains("delet") ||
			pipeline.contains("modif") ||
			pipeline.contains("config") ||
			pipeline.contains("file(s)"));
	}

	private static boolean s_isValidExperimentPipeline(String pipeline) {
		return !(
			pipeline.contains("delet") ||
			pipeline.contains("modif") ||
			pipeline.contains("config") ||
			pipeline.contains("file(s)") ||
			pipeline.contains("executed") ||
			pipeline.contains("folder") ||
			pipeline.contains("remov") ||
			pipeline.contains("file") ||
			pipeline.contains("validat") ||
			pipeline.contains("autorun") ||
			pipeline.contains("pulled") ||
			pipeline.contains("catalog") ||
			pipeline.contains("linked") ||
			pipeline.contains("resource") ||
			pipeline.contains("configur"));
	}

	private static void s_sendEmail(final String subject, final String message, final List<String> recipients) {
		AdminUtils.sendUserHTMLEmail(
			subject,
			message, 
			true, 
			recipients.toArray(new String[recipients.size()]));
	}

	private ArrayList<String> m_getEmailList(UserI user, final String projectId) {
		ArrayList<String> emailList = m_getEmailListRaw(user, projectId);
		Iterator<String> it = emailList.iterator();

		// Filter out any disabled users.
		while (it.hasNext()) {
			if (!_enabledUserEmailList.contains(it.next())) {
				it.remove();
			}
		}
		return emailList;
	}

	private ArrayList<String> m_getEmailListRaw(UserI user, final String projectId) {
		try {
			ArrayList<String> emailList = new ArrayList<String>(_idUtils.notifierList(projectId));

			// Filter out any empty strings.
			while (emailList.remove(""))

			if (emailList.size() < 1) {
				emailList = XnatProjectdata
					.getXnatProjectdatasById(projectId, user, false)
					.getGroupMembers(BaseXnatProjectdata.OWNER_GROUP);
			}
			return emailList;
		} catch (SubjectIdsConfigurationException e) {
			_logger.error("Error getting email recipients: " + e.toString());
			return XnatProjectdata
				.getXnatProjectdatasById(projectId, user, false)
				.getGroupMembers(BaseXnatProjectdata.OWNER_GROUP);
		}
	}

	private boolean m_isInvalidSubjectId(final String subjectLabel, final String projectId) {
		try {
			return !_idUtils.validateSubjectId(subjectLabel, projectId);
		} catch (SubjectIdsConfigurationException | SubjectIdsFormatException | SubjectIdsNonUniqueException e) {
			_logger.error("Error validating subject: " + e.toString());
			return false;
		}
	}

	/**
	 * Retrieves a usable list of mapped values defined in project config.
	 * @param workflow
	 */
	private List<Map<String, String>> m_getConfigMap(WrkWorkflowdata workflow)
	{
		final String wfConfigContents = _configService.getConfigContents(TOOL_NAME, CONFIG_PATH, Scope.Project, workflow.getExternalid());

		// Bail if no config available.
		if (wfConfigContents == null || wfConfigContents == "") {
			return new ArrayList<Map<String, String>>();
		}

		try {
			final List<Map<String, String>> wfConfigList = _gson.fromJson(wfConfigContents, new TypeToken<List<Map<String, String>>>(){}.getType());
			return wfConfigList;
		} catch (Exception e) {
			_logger.error("GSON parser threw an exception: " + e.toString());
			return new ArrayList<Map<String, String>>();
		}
	}
}