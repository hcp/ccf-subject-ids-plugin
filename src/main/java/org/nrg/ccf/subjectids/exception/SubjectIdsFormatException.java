package org.nrg.ccf.subjectids.exception;

/**
 * The Class SubjectIdsFormatException.
 */
@SuppressWarnings("serial")
public class SubjectIdsFormatException extends Exception {

	/**
	 * Instantiates a new subject ids format exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public SubjectIdsFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Instantiates a new subject ids format exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public SubjectIdsFormatException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new subject ids format exception.
	 *
	 * @param message the message
	 */
	public SubjectIdsFormatException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new subject ids format exception.
	 *
	 * @param cause the cause
	 */
	public SubjectIdsFormatException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new subject ids format exception.
	 */
	public SubjectIdsFormatException() {
		super();
	}
	
}
