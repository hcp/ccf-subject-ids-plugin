package org.nrg.ccf.subjectids.exception;

/**
 * The Class SubjectIdsNonUniqueException.
 */
@SuppressWarnings("serial")
public class SubjectIdsNonUniqueException extends Exception {

	/**
	 * Instantiates a new subject ids non unique exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public SubjectIdsNonUniqueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Instantiates a new subject ids non unique exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public SubjectIdsNonUniqueException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new subject ids non unique exception.
	 *
	 * @param message the message
	 */
	public SubjectIdsNonUniqueException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new subject ids non unique exception.
	 *
	 * @param cause the cause
	 */
	public SubjectIdsNonUniqueException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new subject ids non unique exception.
	 */
	public SubjectIdsNonUniqueException() {
		super();
	}
	
}
