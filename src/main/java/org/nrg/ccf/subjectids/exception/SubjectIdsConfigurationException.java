package org.nrg.ccf.subjectids.exception;

/**
 * The Class SubjectIdsConfigurationException.
 */
@SuppressWarnings("serial")
public class SubjectIdsConfigurationException extends Exception {

	/**
	 * Instantiates a new subject ids configuration exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public SubjectIdsConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Instantiates a new subject ids configuration exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public SubjectIdsConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new subject ids configuration exception.
	 *
	 * @param message the message
	 */
	public SubjectIdsConfigurationException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new subject ids configuration exception.
	 *
	 * @param cause the cause
	 */
	public SubjectIdsConfigurationException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new subject ids configuration exception.
	 */
	public SubjectIdsConfigurationException() {
		super();
	}
	
}
