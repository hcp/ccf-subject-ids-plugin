package org.nrg.ccf.subjectids.pojos;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TemplateInfo {
	
	private String template;
	private ArrayList<String> constraintRegex;

}
