package org.nrg.ccf.subjectids.configuration;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

/**
 * The Class CcfSubjectIdsPlugin.
 */
@XnatPlugin(
				value = "ccfSubjectIdsPlugin",
				name = "CCF Subject Ids Plugin"
			)
@ComponentScan({
	"org.nrg.ccf.subjectids.xapi",
	"org.nrg.ccf.subjectids.event.listeners",
	"org.nrg.ccf.subjectids.utils"
	})
public class CcfSubjectIdsPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(CcfSubjectIdsPlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public CcfSubjectIdsPlugin() {
		logger.info("Configuring CCF Subject IDs plugin");
	}
}
