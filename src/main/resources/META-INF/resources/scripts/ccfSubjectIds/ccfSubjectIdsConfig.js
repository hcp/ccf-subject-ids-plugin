
if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.subjectidsconfig === 'undefined') {
	CCF.subjectidsconfig = { updated: false }
}

CCF.subjectidsconfig.queryParams = new URLSearchParams(window.location.search);
CCF.subjectidsconfig.currentProject = CCF.subjectidsconfig.queryParams.get("id");
if (typeof CCF.subjectidsconfig.currentProject == "undefined" || CCF.subjectidsconfig.currentProject == "" ||
	CCF.subjectidsconfig.currentProject == null || CCF.subjectidsconfig.currentProject == "null") {
	CCF.subjectidsconfig.currentProject = XNAT.data.context.project;
}
CCF.subjectidsconfig.CONFIG_URL = "/xapi/projects/" + CCF.subjectidsconfig.currentProject + "/ccfSubjectIds/config";
CCF.subjectidsconfig.SESSIONCONFIG_URL = "/data/projects/" + CCF.subjectidsconfig.currentProject + "/config/subjectIdsSessionLabelValidation/configuration";

CCF.subjectidsconfig.initialize = function() {
	if (typeof CCF.subjectidsconfig.sessionConfig === 'undefined') {
		XNAT.xhr.getJSON({
			url:  serverRoot+CCF.subjectidsconfig.SESSIONCONFIG_URL+"?contents=true",
			success:  function(configJson) {
				CCF.subjectidsconfig.sessionLabelConfig = configJson;
				CCF.subjectidsconfig.populateTable();
			},
			failure:  function() {
				CCF.subjectidsconfig.sessionLabelConfig = [];
				CCF.subjectidsconfig.populateTable();
			}
		})
	}
	if (typeof CCF.subjectidsconfig.configuration === 'undefined') {
		$.ajax({
			type : "GET",
			url:serverRoot+CCF.subjectidsconfig.CONFIG_URL,
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		 })
		.done( function( data, textStatus, jqXHR ) {
			CCF.subjectidsconfig.configuration = data;
			CCF.subjectidsconfig.submitted = false;
			// If we're on the project page, CCF.subjectids will exist and we will initialize and return.  Otherwise,
			// we're on the project settings page, and we should continue and spawn the configuration UI.
			if (typeof CCF.subjectids !== "undefined") {
				CCF.subjectids.initialize();
				return;
			}
			XNAT.spawner.spawn(CCF.subjectidsconfig.spawnConfig()).render($("#subject-labels-list-container")[0]);
			CCF.subjectidsconfig.showRelevantLabelBoxes();
			if (!CCF.subjectidsconfig.validateTemplate()) {
				$("#ccfSubjectIdsTemplate").css("background-color","#FFDDEE");
			} else {
				$("#ccfSubjectIdsTemplate").css("background-color","");
			}

			var currentVal = $("#ccfSubjectIdsChecksum").val();
			$("#ccfSubjectIdsChecksum option[value='']").remove();
			if (typeof currentVal === 'undefined' || currentVal == '') {
				$("#ccfSubjectIdsChecksum").val("LASTDIGITS");
			}
			var enabledVal = $("#ccfSubjectIdsEnabled").val();
			$("#ccfSubjectIdsEnabled option[value='']").remove();
			if (typeof enabledVal === 'undefined' || enabledVal == '') {
				$("#ccfSubjectIdsEnabled").val("false");
			}
			var showGenerateLinkVal = $("#ccfSubjectIdsShowGenerateLink").val();
			$("#ccfSubjectIdsShowGenerateLink option[value='']").remove();
			if (typeof showGenerateLinkVal === 'undefined' || showGenerateLinkVal == '') {
				$("#ccfSubjectIdsShowGenerateLink").val("false");
			}
			$(".panel-footer .submit").mouseover(function(eventObj) {
				CCF.subjectidsconfig.submitted = true;
			});
			$(".panel-footer .submit").click(function(eventObj) {
				CCF.subjectidsconfig.submitted = false;
				CCF.subjectidsconfig.updated = true;
			});
			$("#ccfSubjectIdsTemplate").change(function(eventObj) {
				var segments = CCF.subjectidsconfig.getSegmentsFromInput();
				if (segments.length>0) {
					var modal_opts = { 
				            width: 400,
				            height: 200,
				            title: 'User-supplied segment labels',
				            content: ((segments.length<=5) ? "Please review and/or supply user labels for components of the template that will be supplied by the user" : 
				            				"WARNING:  You have indicated more than five user-supplied segments.  Only five segments are well supported.  " +
									"You will only be able to supply labels for five segments."
							),
				            ok: 'show',
				            okLabel: 'OK',
				            okAction: function(){
								CCF.subjectidsconfig.showRelevantLabelBoxes();
								$("#ccfSubjectIdsSegmentLabel1").focus();
				            },
				            cancel: 'hide',
				            cancelLabel: 'Cancel',
				        };
					xModalOpenNew(modal_opts);
				} else {
					CCF.subjectidsconfig.showRelevantLabelBoxes();
				}
				if (!CCF.subjectidsconfig.validateTemplate()) {
					$("#ccfSubjectIdsTemplate").css("background-color","#FFDDEE");
					xmodal.message("Error","ERROR:  The template contains invalid characters.  They will not be saved with the template.");
					return;
				} else {
					$("#ccfSubjectIdsTemplate").css("background-color","");
				}
				if (CCF.subjectidsconfig.submitted) {
					$(".panel-footer .submit").click();
					CCF.subjectidsconfig.submitted = false;
				}
			});
		})
		.fail( function( data, textStatus, error ) {
			console.log("WARNING:  The configuration call for the CCF Subject IDs plugin returned an error - (", error,")"); 
			CCF.subjectidsconfig.configuration = {};
			XNAT.spawner.spawn(CCF.subjectidsconfig.spawnConfig()).render($("#subject_ids_div")[0]);
			var currentVal = $("#ccfSubjectIdsChecksum").val();
			$("#ccfSubjectIdsChecksum option[value='']").remove();
			if (typeof currentVal === 'undefined' || currentVal == '') {
				$("#ccfSubjectIdsChecksum").val("LASTDIGITS");
			}
			var enabledVal = $("#ccfSubjectIdsEnabled").val();
			$("#ccfSubjectIdsEnabled option[value='']").remove();
			if (typeof enabledVal === 'undefined' || enabledVal == '') {
				$("#ccfSubjectIdsEnabled").val("false");
			}
			var showLinkVal = $("#ccfSubjectIdsShowLink").val();
			$("#ccfSubjectIdsShowLink option[value='']").remove();
			if (typeof showLinkVal === 'undefined' || showLinkVal == '') {
				$("#ccfSubjectIdsShowLink").val("false");
			}
		});
	}
}

CCF.subjectidsconfig.validateTemplate = function() {
	var templVal = $("#ccfSubjectIdsTemplate").val();
	return /^[a-zA-Z0-9._\-?@#$%!{}]+$/.test(templVal); 
}

CCF.subjectidsconfig.refreshData = function() {
	$.ajax({
		type : "GET",
		url:serverRoot+CCF.subjectidsconfig.CONFIG_URL,
		cache: false,
		async: false,
		context: this,
		dataType: 'json'
	 })
	.done( function( data, textStatus, jqXHR ) {
		CCF.subjectidsconfig.configuration = data;
		CCF.subjectidsconfig.submitted = false;
		CCF.subjectidsconfig.updated = false;
	})
	.fail( function( data, textStatus, error ) {
		// Do nothing
	});
}

CCF.subjectidsconfig.showRelevantLabelBoxes = function() {
	var segments = CCF.subjectidsconfig.getSegmentsFromInput();
	$('[id^=ccfSubjectIdsSegmentLabel]').each(function(iter) {
		if ((iter+1)>segments.length) {
			$(this).val('')
			$("div[data-name='" + $(this).attr('id') + "']").hide();
		} else {
			$("div[data-name='" + $(this).attr('id') + "']").show();
		}
	});
}


CCF.subjectidsconfig.getSegmentsFromInput = function() {
	var value = $("#ccfSubjectIdsTemplate").val();
	return this.getSegments(value);
}

CCF.subjectidsconfig.getSegments = function(value) {
	var segments = [];
	var thisSeg = "";
	for (var i=0; i<value.length; i++) {
		var thisChar = value.charAt(i);
		var nextChar = (i<value.length-1) ? value.charAt(i+1) : " ";
		thisSeg+=thisChar;
		if (thisChar !== nextChar) {
			if (thisChar == '@' || thisChar == '#') {
				segments.push(thisSeg);
			}
			thisSeg='';
		}
	}
	return segments;
}

CCF.subjectidsconfig.spawnConfig = function() {

	function configPanel(contents) {
		return {
			id: 'ccfSubjectIdsPanel',
			kind: 'panel.form',
			label: 'Subject IDs Configuration',
			header: false,
			method: "POST",
			// NOTE: ContentType added 2016/09/29.  For some reason the default type (APPLICATION_FORM_URLENCODED_VALUE)
			// began being rejected prior to hitting the XAPI controller.  Even changing method calls to accept objects
			// rather than MultiValueMaps failed to stop 415 errors from being returned.
			contentType: "json",
			action: CCF.subjectidsconfig.CONFIG_URL,
			load: CCF.subjectidsconfig.CONFIG_URL,
			//load: "CCF.subjectidsconfig.configuration",
			refresh: CCF.subjectidsconfig.CONFIG_URL,
			contents: {
				"Enabled": enabled(),
				"ShowLink": showLink(),
				"CheckSum Method": checksum(),
				"Subject ID Template": template(),
				"New ID Template": newTemplate(),
				"Project List": projectList(),
				"Matcth Numeric Only": matchNumeric(),
				"Notifier List": notifierList(),
				"Label1": segmentLabel(1),
				"Label2": segmentLabel(2),
				"Label3": segmentLabel(3),
				"Label4": segmentLabel(4),
				"Label5": segmentLabel(5)
			}
		}
	}
	function enabled() {
		return {
			id: 'ccfSubjectIdsEnabled',
			kind: 'panel.select.init',
			name: 'ccfSubjectIdsEnabled',
			label: 'Enabled?',
			options: {
				"TRUE": {
					 "label": "True",
					 "value": "true"
				},
				"FALSE": {
					 "label": "False",
					 "value": "false"
				}
			}
		}
	}
	function showLink() {
		return {
			id: 'ccfSubjectIdsShowLink',
			kind: 'panel.select.init',
			name: 'ccfSubjectIdsShowLink',
			label: 'Show Generate Link?',
			options: {
				"TRUE": {
					 "label": "True",
					 "value": "true"
				},
				"FALSE": {
					 "label": "False",
					 "value": "false"
				}
			}
		}
	}
	function checksum() {
		return {
			id: 'ccfSubjectIdsChecksum',
			kind: 'panel.select.init',
			name: 'ccfSubjectIdsChecksum',
			label: 'CheckSum Method',
			options: {
				"MOD9710": {
					 "label": "ISO 7064 MOD 97,10",
					 "value": "MOD9710"
				},
				"LASTDIGITS": {
					 "label": "LastDigits from Multiplier",
					 "value": "LASTDIGITS"
				}
			}
		}
	}
	function template() {
		return {
			kind: 'panel.input.text',
			id: 'ccfSubjectIdsTemplate',
			name: 'ccfSubjectIdsTemplate',
			label: 'Subject ID Template',
			value: '',
			placeholder: 'CCF{3456789}!!!!&&',
			description: "The template may contain letters, numbers, dashes, underscores, periods and special characters as follows:" +
					"<br><ul>" +
					"<li>@ - An alphabetic character to be supplied by the user</li>" +
					"<li># - An integer to be supplied by the user</li>" +
					"<li>$ - An alphanumeric character to be supplied by the user</li>" +
					"<li>{###} - A constrained random digit matching listed values</li>" +
					"<li>! - A random digit (the group of random digits will form a random number</li>" +
					"<ul><li>- e.g. 5472 from !!!!<li></ul>" +
					"<li>% - A sequential digit (the group of sequential digits will form a sequential number (e.g. 0001 from @@@@))</li>" +
					"<ul><li>- e.g. 0001 from @@@@</li></ul>" +
					"<li>? - A checksum digit</li>" +
					"</ul>"
					,
			element: {
				onchange: function(){
					// console.log(this.value)
				}
			}
		}
	}
	function newTemplate() {
		return {
			kind: 'panel.input.text',
			id: 'ccfSubjectIdsNewTemplate',
			name: 'ccfSubjectIdsNewTemplate',
			label: 'New ID Template',
			value: '',
			placeholder: 'CCF{345}!!!!&&',
			description: "(OPTIONAL)  Template to use for new ID generation (should be a subset of validation template).  Used to constrain newer ID."
					,
			element: {
				onchange: function(){
					// console.log(this.value)
				}
			}
		}
	}
	function projectList() {
		return {
			kind: 'panel.input.text',
			id: 'ccfSubjectIdsProjectList',
			name: 'ccfSubjectIdsProjectList',
			label: 'Subject ID Projects',
			size: '50',
			value: '',
			description: "A comma-separated list of projects to search for matching labels to exclude."
					,
			element: {
				onchange: function(){
					// console.log(this.value)
				}
			}
		}
	}
	function matchNumeric() {
		return {
			id: 'ccfSubjectIdsMatchNumeric',
			kind: 'panel.select.init',
			name: 'ccfSubjectIdsMatchNumeric',
			label: 'Match Numeric Portion of ID only?',
			options: {
				"TRUE": {
					 "label": "True",
					 "value": "true"
				},
				"FALSE": {
					 "label": "False",
					 "value": "false"
				}
			}
		}
	}
	function notifierList() {
		return {
			kind: 'panel.input.text',
			id: 'ccfSubjectIdsNotifierList',
			name: 'ccfSubjectIdsNotifierList',
			label: 'Email Recipients',
			size: '50',
			value: '',
			description:
				'Comma-separated list of email addresses. ' +
				'This setting is optional. If left empty, ' +
				'notifications are sent to project owner(s).',
			element: {
				onchange: function(){
					// console.log(this.value)
				}
			}
		}
	}
	function segmentLabel(num) {
		var whichone = (function() {switch(num) {
				case 1: return 'first';
				case 2: return 'second';
				case 3: return 'third';
				case 4: return 'fourth';
				case 5: return 'fifth';
				default: return 'xxxx';
				}})();
		return {
			kind: 'panel.input.text',
			id: 'ccfSubjectIdsSegmentLabel' + num,
			name: 'ccfSubjectIdsSegmentLabel' + num,
			label: 'Label for ' + whichone + ' user-defined segment',
			placeholder: 'Subject Age Group (1,2,3,4)',
			/*description: 'Segment <b>TO_BE_ADDED</b>',*/
			element: {
				onchange: function(){
					// console.log(this.value)
				}
			}
		}
	}
	return {
		root: configPanel()
	};	

}


CCF.subjectidsconfig.populateTable = function() {

	var tableHtml = "<table class='data-table xnat-table' style='width:100%;'>";
	tableHtml+="<tr><th></th><th class='center'>XsiType</th><th class='center'>Validation Regex</th><th></th></tr>";
	if (CCF.subjectidsconfig.sessionLabelConfig.length < 1) {
		tableHtml+="<tr><td colspan=4>No session label validation expressions have been configured.</td></tr>";
	}
	for (var i=0; i<CCF.subjectidsconfig.sessionLabelConfig.length; i++) {
		var prconfig = CCF.subjectidsconfig.sessionLabelConfig[i];
		tableHtml+="<tr>";
		tableHtml+="<td><a href=\"javascript:CCF.subjectidsconfig.editConfig('" + prconfig.xsiType + "');\">View/Edit</a></td>";
		tableHtml+="<td>" + prconfig.xsiType + "</td>";
		tableHtml+="<td>" + prconfig.regex + "</td>";
		tableHtml+="<td><button onclick=\"CCF.subjectidsconfig.removeConfig('" + prconfig.xsiType + "');\">Delete</button></td>";
		tableHtml+="</tr>";
	}
	tableHtml+="</table><br><button onclick='CCF.subjectidsconfig.addConfig();'>Add Validation Expression</button>";
	$("#session-labels-header").html(
		'<br><div style="background-color:#EEEEEE;padding: 10px;font-weight: bold;margin-bottom: 20px;">NOTE:  Sessions labels will only be checked if this plugin has been enabled in the "Subject Labels Settings" tab.</div>'
	);
	$("#session-labels-list-container").html(tableHtml);

}

CCF.subjectidsconfig.editConfig = function(xsiType) {

    var addConfig = (typeof xsiType == 'undefined');
    var regex = undefined;

    if (addConfig != true) {
	for (var i=0; i<CCF.subjectidsconfig.sessionLabelConfig.length; i++) {
		var prconfig = CCF.subjectidsconfig.sessionLabelConfig[i];
		if (prconfig.xsiType == xsiType) {
			regex = prconfig.regex;
		}
	}
    }

    xmodal.open({
	title: "Session Label Expression Configuration",
	id: "edit-modal",
	width: 650,
	height: 325,
	content: '<div id="sessionLabelConfig"></div>',
	okLabel: 'Submit',
	okClose: false,
	cancel: 'Cancel',
	cancelAction: (function() {
		// Need to revert any values changed
		//CCF.pvalconfig.populateSettingsTable('#protocol-validation-list-container');
		xmodal.closeAll();
	}),
	okAction: (function() {
		xmodal.closeAll();
		var newXsitype = $("#input-xsiType").val();
		var newRegex = $("#input-regex").val();
		var hasMatch = false;

		for (var i=0; i<CCF.subjectidsconfig.sessionLabelConfig.length; i++) {
			var prconfig = CCF.subjectidsconfig.sessionLabelConfig[i];
			if (prconfig.xsiType == newXsitype) {
				hasMatch = true;
				prconfig.regex = newRegex;
			}
		}
		if (hasMatch == false) {
			CCF.subjectidsconfig.sessionLabelConfig.push({
				"xsiType": newXsitype,
				"regex": newRegex });
		}
		CCF.subjectidsconfig.updateConfig();
	})
    });

console.log("XSITYPE=" + xsiType);
console.log("REGEX=" + regex);

	var viewConfig = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				xsiType: {
					kind: 'panel.input.text',
					id: 'input-xsiType',
					name: 'input-xsiType',
					className: 'required srconfig-ele',
					size: '30',
					label: 'XSI Type:',
					placeholder: 'xnat:mrSessionData',
					description: 'A valid XNAT experiment XSI Type.',
					value: xsiType
				},
				regex: {
					kind: 'panel.input.text',
					id: 'input-regex',
					name: 'input-regex',
					className: 'required srconfig-ele',
					size: '50',
					placeholder: '^SUBJECTID_MR[12]$',
					label: 'Validation Regex:',
					description: 'A valid regular expression.  The sequence SUBJECTID will be replaced with the ' +
							'subject\'s ID in order to support verification for experiment IDs that should ' +
							'contain the subject ID.',
					value: ((typeof regex !== 'undefined') ? regex : '')
				}
			}
		}
	};

	XNAT.spawner.spawn(viewConfig).render($("#sessionLabelConfig")[0]);

	setTimeout(function() {
		if (addConfig != true) {
			$("#input-xsiType").prop('disabled',true);
		}
	}, 100);

}


CCF.subjectidsconfig.removeConfig = function(xsiType) {

	for (var i=0; i<CCF.subjectidsconfig.sessionLabelConfig.length; i++) {
		var prconfig = CCF.subjectidsconfig.sessionLabelConfig[i];
		if (prconfig.xsiType == xsiType) {
			xmodal.confirm({
				title: "Delete Record",
				content: "<h3>Delete this record?</h3><br><table><tr><td style='padding-left:30px'>XSIType:  " + prconfig.xsiType + "</td></tr><tr><td style='padding-left:30px'>Regex: " + prconfig.regex + "</td></tr></table>",
				width: 450,
				height: 250,
				okLabel: "Remove",
				okClose: true,
				okAction: function(dlg){
					console.log("DELETING " + CCF.subjectidsconfig.sessionLabelConfig[i]);
					CCF.subjectidsconfig.sessionLabelConfig.splice(i,1);
					CCF.subjectidsconfig.updateConfig();
				}
			});
			break;
		}
	}

}

CCF.subjectidsconfig.addConfig = function() {

    CCF.subjectidsconfig.editConfig(undefined);

}

CCF.subjectidsconfig.updateConfig = function() {

	console.log(CCF.subjectidsconfig.sessionLabelConfig);

}


CCF.subjectidsconfig.updateConfig = function() {

	XNAT.xhr.put({
		url:  serverRoot+CCF.subjectidsconfig.SESSIONCONFIG_URL,
    		data: JSON.stringify(CCF.subjectidsconfig.sessionLabelConfig),
    		contentType: 'text/plain',
    		success: function(prcJson){
			CCF.subjectidsconfig.populateTable();
		},
    		failure: function(failData){
			xmodal.message("Error posting configuration");
			console.log(failData);
		}
	});

}




